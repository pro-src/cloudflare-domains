Cloudflare Domains
---

[![CSV Validation](https://csvlint.io/validation/5d611c9a4ebc390004000000.svg)](https://csvlint.io/validation/5d611c9a4ebc390004000000)

This repository stores ~2.5 million Cloudflare domains. The raw data
that was used to make these lists was taken from https://securitytrails.com/blog/exploring-cloudflare-public-dns

There were 3.4 million domains in the original data. This data is not the
most recent and thus some domains should no longer be in the list.

The following shows how you might filter out irrevelant domains.
```sh
root@kali:~/wip# git clone https://github.com/christophetd/CloudFlair
root@kali:~/wip# cd CloudFlair/
root@kali:~/wip# pip install -r requirements.txt
root@kali:~/wip/CloudFlair# python
Python 2.7.16+ (default, Jul  8 2019, 09:45:29) 
[GCC 8.3.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import cloudflare_utils
[*] Retrieving Cloudflare IP ranges from https://www.cloudflare.com/ips-v4
>>> import dns.resolver
>>> cloudflare_utils.uses_cloudflare('foobar.com')
False
```

I've provided a list of 1k domains that are currently (August 2019) using Cloudflare's
Anti-Scrape Shield aka JS Challenge (Interstitial).


All of the lists are available via a CDN that supports range requests:

https://cdn.jsdelivr.net/gh/pro-src/cloudflare-domains/0.csv
https://cdn.jsdelivr.net/gh/pro-src/cloudflare-domains/1.csv
https://cdn.jsdelivr.net/gh/pro-src/cloudflare-domains/2.csv
https://cdn.jsdelivr.net/gh/pro-src/cloudflare-domains/3.csv

https://cdn.jsdelivr.net/gh/pro-src/cloudflare-domains/challenges.csv

The csv(s) are newline delimited and each line is whitespace padded. The length
of each line is 21 so you may request a single domain at any given range.

The following shows how to request a random domain:
```py
import requests
import random

session = requests.session()
session.headers.update({
    'Accept-Encoding': 'identity'
})

length = int(session.head(url).headers['content-length'])
count = int(length / 21)
start = random.choice(range(count)) * 21

headers = {
    'Range': 'bytes=%s-%s' % (start, start + 19)
}

domain = session.get(url, headers=headers).text.rstrip()
```

The following shows how to check for the presence of challenges:
```py
import requests
import cfscrape

scraper = cfscrape.create_scraper()

resp = requests.get(domain, headers=scraper.headers)

print('IUAM Challenge: %s' % scraper.is_cloudflare_iuam_challenge(resp))
print('reCAPTCHA: %s' % scraper.is_cloudflare_captcha_challenge(resp))
```
